CREATE TABLE public.barclay_reserved (
    id integer NOT NULL,
    datetime timestamp with time zone DEFAULT now(),
    amount numeric(8,2)
);
