from selenium import webdriver
from selenium.common.exceptions import TimeoutException, NoSuchElementException
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait # available since 2.4.0
from selenium.webdriver.support import expected_conditions as EC # available since 2.26.0
import time
import os
import psycopg2
import datetime
import decimal
import psycopg2
import sys
import traceback

from settings_secret import *


con = psycopg2.connect(PG_CONNECTION_STRING)
cur = con.cursor()


class MaintenanceException(Exception):
    pass


class AnyEC:
    """ Use with WebDriverWait to combine expected_conditions
        in an OR.
    """
    def __init__(self, *args):
        self.ecs = args
    def __call__(self, driver):
        for fn in self.ecs:
            try:
                res = fn(driver)
                if res:
                    return res
            except:
                pass


class AllEC:
    """ Use with WebDriverWait to combine expected_conditions
        in an AND.
    """
    def __init__(self, *args):
        self.ecs = args
    def __call__(self, driver):
        obj = None
        for fn in self.ecs:
            try:
                res = fn(driver)
                if not res:
                    return None
                if res != obj:
                    return None
                res = obj
            except:
                return None


class BarclayAmount():
    @staticmethod
    def to_decimal(raw_amount):
        raw_amount = raw_amount.strip().replace('EUR ', '')
        if raw_amount.endswith(('-', '+')):
            raw_amount = raw_amount[-1] + raw_amount[0:-1]
        return decimal.Decimal(raw_amount.replace(',', '.'))

class BarclayTransaction():
    def __init__(self, valuta, datum, beschreibung, karte, karteninhaber, betrag, zahlplan):
        self.data = dict(valuta=valuta, datum=datum, beschreibung=beschreibung, karte=karte, karteninhaber=karteninhaber, betrag=betrag, zahlplan=zahlplan)
        if self.data.get('betrag').endswith(('-', '+')):
            self.data['betrag'] = self.data['betrag'][-1] + self.data['betrag'][0:-1]
            self.data['betrag'] = decimal.Decimal(self.data.get('betrag').replace(',', '.'))

    def __str__(self):
        return "<BarclayTransaction (%s)>" % (self.data.get('betrag'))


class BarclayDriver(webdriver.Firefox):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.pages = list()
        self.exs = list()
        self.saldo = None
        self.reserved = None
        self.unseen_transactions = None

    def login(self):
        self.get("https://banking.barclaycard.de")
        time.sleep(5)
        if "wartungsarbeit" in self.page_source.lower() or "maintenance" in self.page_source.lower():
            raise MaintenanceException()
        element = WebDriverWait(self, 30).until(EC.presence_of_element_located((By.ID, "loginForm:loginName")))
        element.send_keys(USERNAME)
        element = WebDriverWait(self, 30).until(EC.element_to_be_clickable((By.ID, "loginForm:password")))
        element.send_keys(PASSWORD)
        time.sleep(3)
        button = self.find_element_by_id('loginForm:loginLink')
        button.click()
        time.sleep(5)

    def recieve_and_process_saldo(self):
        """
        return None indicates no change in saldo
        return != None indicates new saldo
        """
        account_table = WebDriverWait(self, 30).until(
                EC.presence_of_element_located(
                    (By.ID, 'j_id97:CreditCardTable:tbody_element')))
        account_row = account_table.find_element_by_tag_name('tr')
        cells = account_row.find_elements_by_tag_name('td')
        saldo = BarclayAmount.to_decimal(cells[2].text.strip())

        cur.execute("SELECT COUNT(*) FROM barclay_saldo")
        num_rows ,= cur.fetchone()
        if num_rows != 0:
            cur.execute("SELECT COALESCE(amount, 0) FROM barclay_saldo ORDER BY id DESC LIMIT 1")
            last_db_saldo ,= cur.fetchone()
        else:
            last_db_saldo = 0
        if last_db_saldo == saldo:
            return

        cur.execute("INSERT INTO barclay_saldo (amount) VALUES (%s)", (saldo,))
        con.commit()
        return saldo

    def find_reserved_cell(self):
        for span in self.find_elements_by_tag_name('span'):
            if span.text == 'Vorgemerkt':
                # skip from span to td to tr
                reserved_element_row = span.find_element_by_xpath('../..')
                return reserved_element_row.find_elements_by_tag_name('td')[1]
        raise NoSuchElementException()

    def recieve_and_process_reserved(self):
        # barclay changes its ids all the time. We need to locate differently
        reserved_raw = self.find_reserved_cell().text.strip()
        reserved = BarclayAmount.to_decimal(reserved_raw)

        cur.execute("SELECT COUNT(*) FROM barclay_reserved")
        num_rows ,= cur.fetchone()
        if num_rows != 0:
            cur.execute("SELECT COALESCE(amount, 0) FROM barclay_reserved ORDER BY id DESC LIMIT 1")
            last_db_reserved ,= cur.fetchone()
        else:
            last_db_reserved = 0
        if last_db_reserved == reserved:
            return

        cur.execute("INSERT INTO barclay_reserved (amount) VALUES (%s)", (reserved,))
        con.commit()
        return reserved

    def go_to_detail(self):
        # click link for transactions
        self.find_element_by_css_selector("[href*='creditCardDetail?acctIndex=0']").click()
        WebDriverWait(self, 30).until(EC.presence_of_element_located((By.ID, 'detailForm')))

    def retrieve_transactions(self):
        tbody = WebDriverWait(self, 30).until(EC.presence_of_element_located((By.ID, 'detailForm:creditUnbillPrimaryTable:tbody_element')))
        rows = tbody.find_elements_by_tag_name('tr')
        transactions = list()
        for row in rows:
            cells = row.find_elements_by_tag_name('td')
            if (len(cells) != 7):
                #  print('cells is not 7, its', len(cells), '. This usually means no transactions are available')
                continue

            valuta = datetime.datetime.strptime(cells[0].text, '%d.%m.%Y').date()
            datum = datetime.datetime.strptime(cells[1].text, '%d.%m.%Y').date()
            beschreibung = item_to_text(cells[2])
            karte = cells[3].text
            karteninhaber = cells[4].text
            betrag = cells[5].text
            zahlplan = cells[6].text
            bc = BarclayTransaction(valuta, datum, beschreibung, karte, karteninhaber, betrag, zahlplan)
            #  print(bc.__dict__, bc)
            transactions.append(bc)

        return transactions

    def process_transactions(self, transactions):
        unseen_transactions = list()
        for t in transactions:

            cur.execute("""
                SELECT EXISTS(
                    SELECT id
                    FROM barclay_transaction
                    WHERE date = %s AND description = %s AND amount = %s
                )""", (t.data.get('datum'), t.data.get('beschreibung'), t.data.get('betrag')))
            exists ,= cur.fetchone()
            if exists:
                continue

            cur.execute("""
                INSERT INTO barclay_transaction (
                    valuta, date, description, card, cardowner, amount, payment_plan
                ) VALUES (%s, %s, %s, %s, %s, %s, %s)""", (
                    t.data.get('valuta'),
                    t.data.get('datum'),
                    t.data.get('beschreibung'),
                    t.data.get('karte'),
                    t.data.get('karteninhaber'),
                    t.data.get('betrag'),
                    t.data.get('zahlplan'),
                )
            )
            unseen_transactions.append(t)
        con.commit()
        return unseen_transactions

    def logout(self):
        self.get('https://banking.barclaycard.de/bir/feature/logout')

    # override
    def quit(self):
        self.logout()
        super().quit()

    def run(self):
        self.exs = list()
        failures = 0
        for _ in range(5):
            try:
                self.login()
                saldo = self.recieve_and_process_saldo()
                if saldo != None:
                    self.saldo = saldo
                self.go_to_detail()
                reserved = self.recieve_and_process_reserved()
                if reserved != None:
                    self.reserved = reserved
                tx = self.retrieve_transactions()
                self.unseen_transactions = self.process_transactions(tx)
                break
            except TimeoutException as te:
                self.exs.append((te, traceback.format_exc(), self.page_source.encode('utf-8')))
                self.logout()
                failures = failures + 1
            except MaintenanceException as me:
                # indicating no failures
                # all of the saldo, reserved, unseen_transactions will be None
                return 0
            # if we reach this, everything went well
        self.quit()
        return failures

    def get_saldo(self):
        return self.saldo

    def get_reserved(self):
        return self.reserved

    def get_unseen_transactions(self):
        return self.unseen_transactions


def item_to_text(item):
    return ''.join([ x.text for x in item.find_elements_by_tag_name('span') ])

def do():
    driver = BarclayDriver()
    failures = driver.run()
    if driver.get_saldo():
        print('new saldo', driver.get_saldo())
    if driver.get_reserved():
        print('new reserved', driver.get_reserved())
    if driver.get_unseen_transactions():
        for t in driver.get_unseen_transactions():
            print('new transaction', t.data.get('datum'), t.data.get('beschreibung'), t.data.get('betrag'))
    # print failures at the end
    if failures == 5:
        print(len(driver.exs), "exceptions", driver.exs, file=sys.stderr)

if __name__ == "__main__":
    os.environ['MOZ_HEADLESS'] = '1'
    do()
