# Retrieving [banking.barclaycard.de](https://banking.barclaycard.de) transactions and track reserved money

This module uses a headless firefox client and selenium to retrieve credit card
transactions. This is neccessary overcome Barclays missing API.

The transactions are stored in a postgres database. If you use cron to execute
the python script, new transactions and changes in saldo and reserved money will
be output to stdout and mailed to you via email.

Of course this all is a **wacky hack**. On the other hand, however, I am not using regex to
parse plain html source.

# Requirements

 - package `virtualenv`
 - package  `firefox`
 - `geckodriver` binary [see link](https://github.com/mozilla/geckodriver/releases)
 - postgresql database >= 9.4 (for jsonb `@>` and `<@` operators)

# Install

Currently, this is no real python module yet so we install it locally in a
virtualenv

 - Install all required packages
 - copy `settings_secret.py.example` to `settings_secret.py` and fill out the
   data
 - set up virtualenv `virtualenv --python=python3 virtualenv`
 - `pip install -r requirements.txt` to install selenium and psycopg2-binary
 - create the database tables `ls *.sql`

# Run

 Via command line
 - `source virtualenv/bin/activate`
 - `python barclay.py`

 Via cron
 - make sure `PATH=/usr/local/bin:/usr/bin:/bin` is set in the crontab (or
   the path to where you copied the geckodriver binary)
 - `*/30 * * * * /home/hbci/git/hbci/virtualenv/bin/python /home/hbci/git/hbci/barclay.py`
   (edit paths of course)
