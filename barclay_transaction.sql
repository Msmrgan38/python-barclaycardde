CREATE TABLE public.barclay_transaction (
    id integer NOT NULL,
    valuta timestamp with time zone,
    date timestamp with time zone,
    description text,
    card text,
    cardowner text,
    amount numeric(8,2),
    payment_plan text
);
